package com.krish.course.CourseApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicService {

	public List<Topic> topics = new ArrayList<Topic>(
			Arrays.asList(new Topic("Core Java", "Core Java", "Intro to Core Java"),
					new Topic("JavaScript", "JavaScript", "Intro to JavaScript"),
					new Topic("Spring AOP", "Spring AOP", "Intro to Spring AOP")));

	public List<Topic> getAllTopics() {
		return topics;
	}

	public Topic getTopicById(String id) {
		return topics.stream().filter(t -> t.id.equalsIgnoreCase(id)).findFirst().get();
	}

	public void addTopic(Topic topic) {
		topics.add(topic);
	}

	public void updateTopic(String id, Topic topic) {
		for (int i = 0; i < topics.size(); i++) {
			if (id.equalsIgnoreCase(topics.get(i).getId())) {
				topics.set(i, topic);
			}
		}
	}

	public void deleteTopicById(String id) {
		topics.removeIf(t -> t.id.equalsIgnoreCase(id));
	}

}
