package com.krish.course.streamPractice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.krish.course.lambdaPractice.Person;

public class StreamUnit {

	public static void main(String[] args) throws IOException {
		
		List<Person> people = Arrays.asList(
				new Person ("Avishek","Das","Durgapur"),
				new Person("Arko","Dey","Housing"),
				new Person("Bani","Das","Mediland"),
				new Person("Shourjya","Ghosh","Bangalore"),
				new Person("Prasenjit","Biswas","Chennai")
				);
		
		//example 1
		people.stream().
		filter(p-> p.getLastName().startsWith("D")).
		forEach(p-> System.out.println(p.getFirstName()));
		
		//example 2
		System.out.println(IntStream.range(1, 5).skip(2).sum());
		
		//example 3
		Person result = people.stream().filter(p->p.getFirstName().startsWith("S")). findFirst().orElse(null);
		System.out.println(result.getCity());
		 
		//example 4
		Stream.of("Eva","Andrew","Tessa","Aba").sorted().findFirst().ifPresent(System.out::println);
		
		//example 5
		Arrays.stream(new int [] {2,4,6,8}).map(x->x*x).max().ifPresent(System.out::println);
		Arrays.stream(new int [] {2,4,6,8}).map(x->x*x).min().ifPresent(System.out::println);
		Arrays.stream(new int [] {2,4,6,8}).map(x->x*x).average().ifPresent(System.out::println);
		
		//example 6
		List<String> singers = Arrays.asList("Sonu Nigam","Arijit singh","Kumar Sanu","Alka Yagnik");
		singers.stream().filter(x-> x.contains("jit")).forEach(s-> System.out.println(s));
		
		//example 7
		Stream<Double> doubleNumbers = Stream.of(7.3,10.2,5.1);
		Double doubleResult = doubleNumbers.reduce(1.0,(a,b)-> a+b).doubleValue();
		System.out.println(doubleResult);
		
		//example 8
		IntSummaryStatistics summary = IntStream.of(6,8,10,12).summaryStatistics();
		System.out.println(summary);
		
		//example 9
		
		List<String> abc = new ArrayList<String>();
		abc.add("1");
		abc.add("2");
		abc.stream().map(x -> Integer.valueOf(x)).forEach(x-> System.out.println(x));
		
	}

}
