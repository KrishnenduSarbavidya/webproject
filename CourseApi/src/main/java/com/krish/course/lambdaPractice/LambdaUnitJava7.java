package com.krish.course.lambdaPractice;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LambdaUnitJava7 {

	
	public static void main (String[] args) {
		
		List<Person> people = Arrays.asList(
				new Person ("Avishek","Das","Durgapur"),
				new Person("Arko","Dey","Housing"),
				new Person("Bani","Das","Mediland"),
				new Person("Shourjya","Ghosh","Bangalore"),
				new Person("Prasenjit","Biswas","Chennai")
				);
		
		//Sort the list
		
		Collections.sort(people, new Comparator<Person>() {

			@Override
			public int compare(Person p1, Person p2) {
				return p1.getFirstName().compareTo(p2.getFirstName());
			}
		});
		
		System.out.println("Printing ther list after sorting by First Name");
		printElementsByCond(people,new Condition() {
			
			@Override
			public boolean filter(Person p) {
				return true;
			}
		});
		
		System.out.println("Printing ther list after applying condition");
		printElementsByCond(people,new Condition() {
			
			@Override
			public boolean filter(Person p) {
				return p.getFirstName().startsWith("A");
			}
		});
		
	}

	/*
	 * public static void printAllelements(List<Person> people) { for (Person p :
	 * people) { System.out.println("Printing elements::"+ p); } }
	 */
	
	public static void printElementsByCond(List<Person> people,Condition condition) {
		for (Person p : people) {
			if (condition.filter(p)) {
				System.out.println(p);
			}
		}
	}
	
	interface Condition {
		boolean filter(Person p);
	}
	
}
