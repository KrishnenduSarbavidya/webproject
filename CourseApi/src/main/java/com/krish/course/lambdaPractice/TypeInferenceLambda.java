package com.krish.course.lambdaPractice;

public class TypeInferenceLambda {

	public static void main(String[] args) {
		
		printLambda(abc -> abc.length());
	}
	
	public static void printLambda(stringLengthLambda s) {
		System.out.println(s.getLength("Learning Lambda"));
	}
	
	interface stringLengthLambda {
		int getLength(String s);
	}
}
