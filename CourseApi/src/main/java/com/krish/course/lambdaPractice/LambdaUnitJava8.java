package com.krish.course.lambdaPractice;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class LambdaUnitJava8 {

	
	public static void main (String[] args) {
		
		List<Person> people = Arrays.asList(
				new Person ("Avishek","Das","Durgapur"),
				new Person("Arko","Dey","Housing"),
				new Person("Bani","Das","Mediland"),
				new Person("Shourjya","Ghosh","Bangalore"),
				new Person("Prasenjit","Biswas","Chennai")
				);
		
		//Sort the list
		
		Collections.sort(people, (p1,p2) -> p1.getFirstName().compareTo(p2.getFirstName()));
		
		System.out.println("Printing the list after sorting by First Name");
		
		printElementsByCond(people,p -> true);
		
		System.out.println("Printing the list after applying condition");
		
		printElementsByCond(people,p -> p.getFirstName().startsWith("P"));
		
	}

	public static void printElementsByCond(List<Person> people,Predicate<Person> condition) {
		for (Person p : people) {
			if (condition.test(p)) {
				System.out.println(p);
			}
		}
	}
	
}
