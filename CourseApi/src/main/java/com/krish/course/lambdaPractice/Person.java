package com.krish.course.lambdaPractice;

public class Person {

	private String firstName;
	private String LastName;
	private String city;
	
	public Person(String firstName, String lastName, String city) {
		super();
		this.firstName = firstName;
		LastName = lastName;
		this.city = city;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", LastName=" + LastName + ", city=" + city + "]";
	}
	
}
