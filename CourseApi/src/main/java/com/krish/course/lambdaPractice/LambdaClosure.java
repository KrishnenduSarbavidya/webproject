package com.krish.course.lambdaPractice;

public class LambdaClosure {
	
	public static void main(String[] args) {
		
		int a = 10;
		int b = 12; //final no need to mention since it is used in 
		//lambda compiler freezes it's value (effectively final)
		doProcess(a, i -> System.out.println(i+b));
	}

	private static void doProcess(int a, Process p) {
		p.pleaseProcess(a);
	}

	interface Process {
		void pleaseProcess(int i);
	}
}
