package com.krish.course.lambdaPractice;

import java.util.function.BiConsumer;

public class LambaExceptionHandlingJava8 {

	public static void main(String[] args) {
		
		int[] numbers = {1,2,4,6,8,10};
		int key = 0;
		
		processNumbers(numbers,key,warapperLambda((n , k) -> System.out.println(n/k)));

	}

	private static void processNumbers(int[] numbers, int key, BiConsumer<Integer, Integer> consumer) {
		for (int num : numbers)
			consumer.accept(num, key);
	}
	
	private static BiConsumer<Integer, Integer> warapperLambda(BiConsumer<Integer, Integer> consumer) {
		return (a,b) -> {
			try {
				consumer.accept(a, b);
			} catch (ArithmeticException e) {
				System.err.println("Exception caught in wrapper Lambda");
			}
		};
	}

}
